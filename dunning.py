#This file is part of Tryton.  The COPYRIGHT file at the top level
#of this repository contains the full copyright notices and license terms.

#TODO Check if another state 'running' between confirmed and closed is needed.

'Dunning'
import datetime
import copy
import base64
from decimal import Decimal
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.wizard import Wizard
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool


ZERO = Decimal('0.0')


class DunningLevel(ModelSQL, ModelView):
    'Dunning Level'
    _name = 'account.dunning.level'
    _description = __doc__

    charge = fields.Numeric('Charge', digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'],
            help='Defines a late payment charge for this dunning level.')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')

    def default_currency_digits(self):
        return 2

    def get_currency_digits(self, ids, names):
        '''
        This method try to get the currency digits from
        * the company in the context of the actual user or
        * the company with the smallest id (first created)'''
        company_obj = Pool().get('company.company')
        res = {}
        default_currency_digits = 2
        currency_digits = default_currency_digits

        if Transaction().context.get('company', 0) > 0:
            company = company_obj.browse(Transaction().context['company'])
        else:
            company_id = company_obj.search([], limit=1,
                    order=[('id', 'ASC')])[0]
            company = company_obj.browse(company_id)
        if company and company.currency.digits:
            currency_digits = company.currency.digits

        for level in self.browse(ids):
            for name in names:
                res.setdefault(name, {})
                res[name].setdefault(level.id, currency_digits)
        return res

DunningLevel()


class Dunning(ModelSQL, ModelView):
    'Dunning'
    _name = 'account.dunning'

    charge = fields.Function(fields.Numeric('Charge',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits', 'level'],
            help='The late payment charges for this level.'), 'get_charge')

    def get_charge(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        res = {}
        for dunning in self.browse(ids):
            charge = ZERO
            if 'charge' in dunning.level and dunning.lines:
                charge = dunning.level.charge
            res[dunning.id] = currency_obj.round(dunning.company.currency,
                    charge)
        return res

    def get_total(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        charge = ZERO
        res = super(Dunning, self).get_total(ids, name)
        for dunning in self.browse(ids):
            if res[dunning.id] != ZERO:
                charge += dunning.level.charge
        res[dunning.id] += currency_obj.round(dunning.company.currency, charge)
        return res

    def _on_change_lines_payments(self, vals):
        level_obj = Pool().get('account.dunning.level')
        level_id = vals.get('level', False)
        level = level_obj.browse(level_id)
        level_charge = level.charge

        res = super(Dunning, self)._on_change_lines_payments(vals)

        res = res.copy()
        res.setdefault('charge', ZERO)
        if vals.get('lines') and res.get('amount_payable') > ZERO:
            res['charge'] += level_charge
            res['total'] += level_charge
        return res

Dunning()


