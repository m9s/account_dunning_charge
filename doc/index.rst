account_dunning_charge Module
#############################

This document describes the general concepts of the account_dunning_charge 
module. All widgets in this module have a tool-tip help which is shown, when
the mouse is over a special widget.

The account_dunning_charge module provides additional late payment charges
for the dunning terms.

Configuration
=============

The charge can be set in the dunning term:

 * Financial Management > Configuration > Dunning > Dunning Terms



Charges in dunnings
===================

The charge is evaluated for each dunning on the actual level. 

Note: If an accumulated value of past charges is needed, the charge for 
each level can be a sum of the actual level charges plus all previous 
levels charges in the level sequence of a term.

The charge is added to the total amount to pay.


